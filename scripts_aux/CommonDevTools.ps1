
# tools we expect devs across many scenarios will want
choco install -y vscode
choco install -y git --package-parameters="'/GitAndUnixToolsOnPath /WindowsTerminal'"
choco install -y python2
choco install -y hyper
choco install -y postman
choco install -y 7zip.install
choco install -y sysinternals
