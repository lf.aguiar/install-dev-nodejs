# Instalação de ambiente nodejs backend para windows automatizado

projeto inteiramente baseado e inspirado no dev kit da Microsoft: https://github.com/microsoft/windows-dev-box-setup-scripts

# Problemas com permissão no Power Shell?
tente os passos abaixo: 
### Verificar políticas de permissão de scripts do powershell
> get-executionpolicy -list

### Setar permissão para execução de scripts
> set-executionpolicy unrestricted -scope <scopo_escolhido>

# Executar setup.bat como adm
> setup.bat