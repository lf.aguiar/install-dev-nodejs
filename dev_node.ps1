# Description: Boxstarter Script
# Author: Microsoft
# Modified by luis.aguiar
# Common settings for web development with NodeJS


Disable-UAC

$ConfirmPreference = "None" #ensure installing powershell modules don't prompt on needed dependencies

# Get the base URI path from the ScriptToCall value

$helperUri = $PSScriptRoot + "/scripts_aux"
write-host "helper script base URI is $helperUri"

function executeScript {
    Param ([string]$script)
    write-host "executing $helperUri/$script ..."
    Invoke-Expression ((new-object net.webclient).DownloadString("$helperUri/$script"))
}

#--- Setting up Windows ---
executeScript "SystemConfiguration.ps1";
executeScript "FileExplorerSettings.ps1";
executeScript "CommonDevTools.ps1";
executeScript "Browsers.ps1";
#executeScript "WSL.ps1";


#--- VS code extension ---
code --install-extension naumovs.color-highlight
code --install-extension mikestead.dotenv
code --install-extension dracula-theme.theme-dracula
code --install-extension editorconfig.editorconfig
code --install-extension dbaeumer.vscode-eslint
code --install-extension esbenp.prettier-vscode
code --install-extension rocketseat.rocketseatreactnative
code --install-extension rocketseat.rocketseatreactjs
code --install-extension visualstudioexptteam.vscodeintellicode
code --install-extension vscode-icons-team.vscode-icons

#--- Set Settings custom VSCode
executeScript "VSCodeConfiguration.ps1";

#--- Tools ---
choco install -y nodejs-lts # Node.js LTS, Recommended for most users
# choco install -y nodejs # Node.js Current, Latest features
# choco install -y python2 # Node.js requires Python 2 to build native modules

Enable-UAC
#Enable-MicrosoftUpdate
#Install-WindowsUpdate -acceptEula
